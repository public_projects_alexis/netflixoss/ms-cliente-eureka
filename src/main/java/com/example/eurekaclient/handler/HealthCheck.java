package com.example.eurekaclient.handler;

import com.netflix.appinfo.HealthCheckHandler;
import com.netflix.appinfo.InstanceInfo;
import org.springframework.stereotype.Component;

/**
 * Podríamos definir un método REST securizado para cambiarlo (el estado),
 * o recuperarlo de una base de datos, o de una caché,
 * incluso podríamos definirlo como una propiedad de configuración
 * y cambiarla en caliente gracias a cloud-config.
 */
@Component
public class HealthCheck implements HealthCheckHandler {
    private int counter = -1;

    @Override
    public InstanceInfo.InstanceStatus getStatus(InstanceInfo.InstanceStatus currentStatus) {
        counter++;

        switch (counter){
            case 0:
                return InstanceInfo.InstanceStatus.OUT_OF_SERVICE;
            case 1:
                return InstanceInfo.InstanceStatus.DOWN;
            case 2:
                return InstanceInfo.InstanceStatus.STARTING;
            case 3:
                return InstanceInfo.InstanceStatus.UNKNOWN;

                default:
                    return InstanceInfo.InstanceStatus.UP;
        }

    }
}
