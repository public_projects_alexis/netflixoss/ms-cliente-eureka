package com.example.eurekaclient.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class Book {
    private Integer id;
    private String name;
    private BigDecimal price;
}
